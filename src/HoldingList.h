//
// Created by caina on 10/05/16.
// HoldingList is the class of the secundary list used for storing nodes reached by active threads on a given region while it remains inactive. This list does not
// allow two nodes with the same coordinates and keeps track of the highest priority value between its nodes.
//

#ifndef ASTAR_MSA_HOLDINGLIST_H
#define ASTAR_MSA_HOLDINGLIST_H

#include <limits>
#include <map>
#include <vector>

#include "Coord.h"
#include "Node.h"

template < int N >
class HoldingList{
public:

    HoldingList();

    int get_highest_priority() const;
    void conditional_insert(const Node<N>& n);
    void consume(std::vector<Node<N>>& node_list);

    void update_priority(const int new_priority);
    void clear();
    int size();


    std::map<Coord<N>, Node<N>> nodes;
private:

    int highest_priority;
};

template < int N >
HoldingList<N>::HoldingList() : highest_priority(std::numeric_limits<int>::max()){

}
template < int N >
int HoldingList<N>::get_highest_priority() const {
    return highest_priority;
}
template < int N >
void HoldingList<N>::conditional_insert(const Node<N> &n) {
    auto it = nodes.find(n.pos);
    if (it == nodes.end() || n.hasBetterPriorityThan(it->second)) {
        nodes[n.pos] = n;
        update_priority(n.get_f());
    }
}

template< int N>
void HoldingList<N>::consume(std::vector<Node<N>>& node_list){
    for(auto it = nodes.begin(); it != nodes.end(); ++it){
        node_list.push_back(it->second);
    }
    nodes.clear();
    highest_priority = std::numeric_limits<int>::max();
}

template<int N>
void HoldingList<N>::update_priority(const int new_priority) {
    highest_priority = std::min(highest_priority, new_priority);
}

template < int N >
void HoldingList<N>::clear() {
    nodes.clear();
}
template < int N >
int HoldingList<N>::size() {
    return (int) nodes.size();
}


#endif //ASTAR_MSA_HOLDINGLIST_H