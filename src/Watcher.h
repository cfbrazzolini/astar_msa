//
// Created by caina on 25/04/16.
//

#ifndef ASTAR_MSA_WATCHER_H
#define ASTAR_MSA_WATCHER_H

#include <atomic>
#include <cmath>
#include <iostream>
#include <map>
#include <mutex>
#include <set>
#include <vector>


#include "Node.h"
#include "WatcherBase.h"


template<int N>
class Watcher : public WatcherBase {
public:

    static Watcher* getInstance();
    void setAttributes(int threadsNum_, int totalRegions, std::map<Coord<N>, Node<N> > *ClosedList_, std::mutex *closed_list_mutex_,
                       unsigned long long max_memory,double memory_threshold);
    void updateMemoryOccupation(int rid);
    unsigned long long int getMaxMemoryOccupied();
    State getState();

    Watcher(Watcher const&) = delete;
    void operator=(Watcher const&)  = delete;



private:
    bool active;
    int threadsNum;
    unsigned long long int THRESHOLD;
    unsigned long long int MAX_MEMORY;
    unsigned long long int memoryOccupation;
    unsigned long long int maxMemoryOccupied;

    std::atomic<State> state;

    std::mutex *closed_list_mutex;
    std::mutex memoryOccupationMutex;
    std::map<Coord<N>, Node<N>> *ClosedList;
    std::vector<unsigned long long int> closedListsSizes;

    Watcher();
    ~Watcher() { };
};


#endif //ASTAR_MSA_WATCHER_H
