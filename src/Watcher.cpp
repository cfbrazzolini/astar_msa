//
// Created by caina on 25/04/16.
//

#include "Watcher.h"

template<int N>
Watcher<N>::Watcher(){
    active = false;
    threadsNum = 1;
    memoryOccupation = 0;
    maxMemoryOccupied = 0;

    state.store(MONITORING);
}


template<int N>
Watcher<N>* Watcher<N>::getInstance() {
    static Watcher<N> instance;
    return &instance;
}


template<int N>
void Watcher<N>::setAttributes(int threadsNum_, int totalRegions, std::map<Coord<N>, Node<N> > *ClosedList_, std::mutex
*closed_list_mutex_,unsigned long long max_memory,double memory_threshold){

    threadsNum = threadsNum_;
    ClosedList = ClosedList_;
    closed_list_mutex = closed_list_mutex_;
    closedListsSizes.assign((size_t)totalRegions,0);

    if(max_memory != 0){
        active = true;
        THRESHOLD = (unsigned long long )std::floor(max_memory * memory_threshold);
        MAX_MEMORY = max_memory;
    }
}

template< int N>
void Watcher<N>::updateMemoryOccupation(int rid) {
    unsigned long long int dif = ClosedList[rid].size() - closedListsSizes[rid];
    closedListsSizes[rid] = ClosedList[rid].size();

    std::unique_lock<std::mutex> memory_lock(memoryOccupationMutex);

    memoryOccupation += dif * sizeof(Node<N>);

    if(memoryOccupation > maxMemoryOccupied){
        maxMemoryOccupied = memoryOccupation;
    }
    //std::cout << "Occupation: " << memoryOccupation <<std::endl;

    if(!active){
        return;
    }

    if(getState() == MONITORING && memoryOccupation >= THRESHOLD){
        state.store(MOVEMENT);
        //std::cout << "Movement state" << std::endl;
    }
    else if(getState() == MOVEMENT && memoryOccupation < THRESHOLD){
        state.store(MONITORING);
        //std::cout << "Monitoring state" << std::endl;
    }

    if(memoryOccupation > MAX_MEMORY){
        static int error_msg = 0;
        if (error_msg == 0){
            ++error_msg;
            std::cout << "Ultrapassou o limite maximo de ocupação " << std::endl;
        }
    }


}

template<int N>
unsigned long long int Watcher<N>::getMaxMemoryOccupied() {
    return maxMemoryOccupied;
}

template<int N>
typename WatcherBase::State Watcher<N>::getState() {
    return state.load();
}

#define WATCHER_DECLARE_TEMPLATE(X) \
template class Watcher< X >; \


MAX_NUM_SEQ_HELPER(WATCHER_DECLARE_TEMPLATE);
