/*!
 * \author Daniel Sundfeld
 * \copyright MIT License
 */
#include "PAStar.h"

#include <sched.h>
#include <atomic>
#include <condition_variable>
#include <iostream>
#include <thread>
#include <vector>

#include "backtrace.h"
#include "Coord.h"
#include "Node.h"
#include "TimeCounter.h"

template < int N >
PAStar<N>::PAStar(const Node<N> &node_zero, const struct PAStarOpt &opt)
: m_options(opt)
{
/*    std::cout << "Running PAStar with: "
              << opt.threads_num << " threads, "
              << opt.regions_per_thread << " regions per thread, "
              << Coord<N>::get_hash_name() << " hash, "
              << Coord<N>::get_hash_shift() << " shift\n";*/

    std::cout << "T: " << opt.threads_num << std::endl;
    std::cout << "R: " << opt.regions_per_thread << std::endl;

    if(opt.memory_limit != 0){
        std::cout << "L: " << opt.memory_limit << std::endl;
        std::cout << "O: " << (unsigned long long )std::floor(opt.memory_limit  * opt.memory_threshold) << std::endl;
    }


    end_cond = false;
    sync_count = 0;
    final_node.set_max();

    OpenList = new PriorityList<N>[m_options.total_regions]();
    ClosedList = new std::map< Coord<N>, Node<N> >[m_options.total_regions]();
    holdingList = new HoldingList<N>[m_options.total_regions]();

    write_timer = new std::chrono::duration<int,std::milli>[m_options.total_regions]();
    nodes_reopen = new long long int[m_options.total_regions]();
    nodes_open_rewrite = new long long int[m_options.total_regions]();

    queue_mutex = new std::mutex[m_options.total_regions]();
    queue_condition = new std::condition_variable[m_options.total_regions]();
    queue_nodes = new std::vector< Node<N> >[m_options.total_regions]();

    activeRegions.assign((size_t)m_options.threads_num,-1);
    locations.assign((size_t)m_options.total_regions,Location::MEMORY);
    least_prioritary_regions.assign((size_t)m_options.threads_num,-1);

    // Enqueue first node
    OpenList[0].enqueue(node_zero);


    closed_list_mutex = new std::mutex[m_options.total_regions]();

    Watcher<N>::getInstance()->setAttributes(m_options.threads_num, m_options.total_regions, ClosedList, closed_list_mutex,m_options
            .memory_limit,m_options.memory_threshold);
}

template < int N >
PAStar<N>::~PAStar()
{
    delete[] OpenList;
    delete[] ClosedList;
    delete[] write_timer;
    delete[] nodes_reopen;
    delete[] nodes_open_rewrite;
    delete[] queue_mutex;
    delete[] queue_condition;
    delete[] queue_nodes;
}

template < int N >
int PAStar<N>::set_affinity(int tid)
{
    cpu_set_t mask;
    CPU_ZERO(&mask);
    CPU_SET(1 << tid, &mask);
    return sched_setaffinity(0, sizeof(mask), &mask);
}

/*!
 * Add a vector of nodes \a nodes to the OpenList with id \a rid. Use the
 * ClosedList information to ignore expanded nodes.
 * This function is a expensive function and should be called with no locks.
 * Parallel access should never occur on OpenList and ClosedList with
 * same rids.
 */
template < int N >
void PAStar<N>::enqueue(int rid, std::vector< Node<N> > &nodes)
{
    typename std::map< Coord<N>, Node<N> >::iterator c_search;

    for (typename std::vector< Node<N> >::iterator it = nodes.begin() ; it != nodes.end(); ++it)
    {
        if ((c_search = ClosedList[rid].find(it->pos)) != ClosedList[rid].end())
        {
            if (it->get_g() >= c_search->second.get_g())
                continue;
            ClosedList[rid].erase(it->pos);
            nodes_reopen[rid] += 1;
        }
        //std::cout << Adding:\t" << *it << std::endl;
        OpenList[rid].conditional_enqueue(*it, &(nodes_open_rewrite[rid]));
    }
    return;
}

//! Consume the queue with id \a rid
template < int N >
void PAStar<N>::consume_queue(int rid)
{
    std::unique_lock<std::mutex> queue_lock(queue_mutex[rid]);
    std::vector< Node<N> > nodes_to_expand(queue_nodes[rid]);
    queue_nodes[rid].clear();
    queue_lock.unlock();

    enqueue(rid, nodes_to_expand);
    return;
}

//! Wait something on the queue
template < int N >
void PAStar<N>::wait_queue(int tid)
{
    std::unique_lock<std::mutex> queue_lock(queue_mutex[tid]);
    if (queue_nodes[tid].size() == 0)
        queue_condition[tid].wait(queue_lock);
    return;
}

//! Wake up everyone waiting on the queue
template < int N >
void PAStar<N>::wake_all_queue()
{
    for (int i = 0; i < m_options.total_regions; ++i)
    {
        std::unique_lock<std::mutex> queue_lock(queue_mutex[i]);
        queue_condition[i].notify_one();
    }
    return;
}

//! Sync all threads
template < int N >
void PAStar<N>::sync_threads()
{
    std::unique_lock<std::mutex> sync_lock(sync_mutex);
    if (++sync_count < m_options.threads_num)
        sync_condition.wait(sync_lock);
    else
    {
        sync_count = 0;
        sync_condition.notify_all();
    }
}

template < int N >
void PAStar<N>::write(int rid){

    locations[rid] = Location::MEM_2_DISK;

    std::string file_name("./files/" + std::to_string(rid) + ".txt");
    std::ofstream out_file(file_name,std::ofstream::out | std::ofstream::trunc);

    if(!out_file.is_open()){
        std::cout << "Can't open output file " << file_name << std::endl;
        exit(-1);
    }

    locations[rid] = Location::MEM_2_DISK;

    auto begin = std::chrono::high_resolution_clock::now();
    for(auto it = ClosedList[rid].begin();it != ClosedList[rid].end(); ++it){
        Node<N>&  n = it->second;
        std::stringstream temp;
        temp << n.pos;
        std::string str = temp.str();

        str.erase(str.end()-1);
        str.erase(str.begin());

        out_file << n.get_g() << "\n" << n.get_parenti() << "\n" << str << std::endl;
    }
    auto end = std::chrono::high_resolution_clock::now();
    write_timer[rid] += std::chrono::duration_cast< std::chrono::duration<int,std::milli> >(end - begin);

    /*std::unique_lock<std::mutex> print_guard(print_mutex);
    std::cout << "W " << rid << "\n\t Nos escritos: " << ClosedList[rid].size() << std::endl;
    print_guard.unlock();*/

    ClosedList[rid].clear();

    locations[rid] = Location::DISK;
}

template < int N >
void PAStar<N>::read(int rid){
    locations[rid] = Location::DISK_2_MEM;

    std::string file_name("./files/" + std::to_string(rid) + ".txt");
    std::ifstream in_file(file_name);

    if(!in_file.is_open()){
        std::cout << "Can't open input file " << file_name << std::endl;
        exit(-1);
    }

    locations[rid] = Location::DISK_2_MEM;

    while(!in_file.eof()){

        int g,parenti;
        Coord<N> pos;

        in_file >> g;
        in_file >> parenti;

        for(int i = 0; i < N;i++){
            in_file >> pos[i];
        }

        Node<N> n(g,pos,parenti);
        ClosedList[rid][n.pos] = n;


    }

   /* std::unique_lock<std::mutex> print_guard(print_mutex);
    std::cout << "R " << rid << "\n\t Nos lidos: " << ClosedList[rid].size() << std::endl;
    print_guard.unlock();*/

    locations[rid] = Location::MEMORY;
}

template < int N >
bool PAStar<N>::check_node_barrier(int rid)
{
    if (OpenList[rid].empty())
    {
        wait_queue(rid);
        return true;
    }

    int min_priority = std::numeric_limits<int>::max();

    for(int id = 0; id < m_options.total_regions; ++id)
        min_priority = std::min(OpenList[id].get_highest_priority(), min_priority);

    float local_priority = OpenList[rid].get_highest_priority();
    float global_priority = ((float)min_priority) * 1.1f;
    if (local_priority > global_priority)
        return true;

    return false;
}

//! Execute the pa_star algorithm until all nodes expand the same final node
template < int N >
void PAStar<N>::worker_inner(int tid, const Coord<N> &coord_final)
{
    int arID;
    Node<N> current;
    Watcher<N> * watcher = Watcher<N>::getInstance();
    std::vector< Node<N> > *neigh = new std::vector< Node<N> >[m_options.total_regions];

    // Loop ended by process_final_node
    while (end_cond == false)
    {

        for(int rid = tid; rid < m_options.total_regions; rid += m_options.threads_num){
            if(rid == activeRegions[tid]){
                consume_queue(rid);
            }
            else{
                consume_queue_to_holding_list(rid);
            }
        }

        if(watcher->getState() == WatcherBase::MOVEMENT){

            int selectedRegionID = getLeastPrioritaryRegion(tid);

            if(selectedRegionID != -1){
                write(selectedRegionID);
                watcher->updateMemoryOccupation(selectedRegionID);
            }
        }


        if(updateHighestPriorityRegionOnThread(tid)){
            // New highest priority region has been identified

            if(locations[activeRegions[tid]] == Location::DISK){
                read(activeRegions[tid]);
            }
            consume_holding_list(activeRegions[tid]);
        }

        typename std::map< Coord<N>, Node<N> >::iterator c_search;
        arID = activeRegions[tid];

        if (check_node_barrier(arID))
            continue;

        watcher->updateMemoryOccupation(arID);

        // Start phase
        // Reduce the queue
        // consume_queue(tid);

        // Dequeue phase
        if (OpenList[arID].dequeue(current) == false)
        {
            wait_queue(arID);
            continue;
        }

        // Check if better node is already found
        if ((c_search = ClosedList[arID].find(current.pos)) != ClosedList[arID].end())
        {
            if (current.get_g() >= c_search->second.get_g())
                continue;
            nodes_reopen[arID] += 1;
        }

        //std::cout << "[" << tid << "] Opening node:\t" << current << std::endl;
        ClosedList[arID][current.pos] = current;

        if (current.pos == coord_final)
        {
            process_final_node(tid, current);
            continue;
        }

        // Expand phase
        current.getNeigh(neigh, m_options.total_regions);

        // Reconciliation phase
        for (int i = 0; i < m_options.total_regions; i++)
        {
            if (i == arID)
                enqueue(arID, neigh[arID]);
            else if (neigh[i].size() != 0)
            {
                std::unique_lock<std::mutex> queue_lock(queue_mutex[i]);
                queue_nodes[i].insert(queue_nodes[i].end(), neigh[i].begin(), neigh[i].end());
                queue_condition[i].notify_one();
            }
            neigh[i].clear();
        }
    }
    delete[] neigh;
    return;
}

/*!
 * Process \a n as an possible answer. Check end phase 1.
 * When a final node is first opened, it is broadcasted in all OpenLists.
 * When all OpenList open this node, it have the lowest priority among all
 * openlists, then it must proceed to Check end phase 2.
 * This is functions does not require synchronization between the threads.
 */
template < int N >
void PAStar<N>::process_final_node(int tid, const Node<N> &n)
{
    std::unique_lock<std::mutex> final_node_lock(final_node_mutex);

    // Better possible answer already found, discard n
    if (final_node.get_f() < n.get_f())
        return;

    if (n.pos.get_id(m_options.threads_num) == (unsigned int)tid)
    {
        //std::cout << "[" << tid << "] Possible answer found: " << n << std::endl;
        // Broadcast the node
        final_node = n;
        final_node_count = 0;
        final_node_lock.unlock();

        for (int i = 0; i < m_options.threads_num; i++)
        {
            if (i != tid)
            {
                int arID = activeRegions[i]; // thread Active Region ID
                std::unique_lock<std::mutex> queue_lock(queue_mutex[arID]);
                queue_nodes[arID].push_back(n);
                queue_condition[arID].notify_one();
            }
        }
    }
    else
    {
       //std::cout << "[" << tid << "] Agreed with possible answer! " << n << "/" << final_node << std::endl;
       //if (n != final_node) std::cout << "BUG HERE!\n";
       final_node_lock.unlock();
    }

    // Process a broadcast node
    if (++final_node_count == m_options.threads_num)
    {
        // This node have the highest priority between all Openlist.
        end_cond = true;
        return;
    }
    return;
}

/*!
 * Check end phase 2.
 * After everyone agreed that a possible answer is found, we must syncronize
 * the threads, consume the queue and check again, if the answer have the
 * lowest priority between all OpenLists
 * The queue consume and/or thread scheduling might have caused the final_node
 * to not have the lowest priority.
 * This is a very costly function, threads syncronization are called twice.
 */
template < int N >
bool PAStar<N>::check_stop(int tid)
{
    wake_all_queue();
    sync_threads();
    Node<N> n = final_node;
    int arID = activeRegions[tid];
    consume_queue(arID);
    if (OpenList[arID].get_highest_priority() < final_node.get_f())
    {
        //std::cout << "[" << tid << "] reporting early end!\n";
        end_cond = false;
    }
    sync_threads();
    if (end_cond == false)
    {
        ClosedList[arID].erase(n.pos);
        if (n.pos.get_id(m_options.threads_num) == (unsigned int)tid)
            OpenList[arID].conditional_enqueue(n);
        return true;
    }
    return false;
}

//! Execute a worker thread. This thread have id \a tid
template < int N >
int PAStar<N>::worker(int tid, const Coord<N> &coord_final)
{
    set_affinity(tid);
    activeRegions[tid] = tid;
    // worker_inner is the main inner loop
    // check_stop syncs and check if is the optimal answer
    do {
        worker_inner(tid, coord_final);
    } while (check_stop(tid));

    return 0;
}

template < int N >
void PAStar<N>::print_nodes_count()
{
    long long int nodes_total = 0;
    long long int open_list_total = 0;
    long long int holding_list_total = 0;
    long long int closed_list_total = 0;
    long long int nodes_reopen_total = 0;
    long long int nodes_open_rewrite_total = 0;

    std::cout << "Total nodes count:" << std::endl;
    for (int i = 0; i < m_options.total_regions; ++i)
    {
        long long int total_region = OpenList[i].size() + ClosedList[i].size() + nodes_reopen[i];
        std::cout << "rid " << i
             << "\t" << OpenList[i].get_highest_priority()
             << "\tOpenList: " << OpenList[i].size()
             << "\tHoldingList: " << holdingList[i].size()
             << "\tClosedList: " << ClosedList[i].size()
             << "\tReopen: " << nodes_reopen[i]
             << "\tTotal: " << total_region
             << "\tWriteTimer: " << write_timer[i].count()
             << "\t(Open Rewrite: " << nodes_open_rewrite[i] << ")\n";
        open_list_total += OpenList[i].size();
        holding_list_total += holdingList[i].size();
        closed_list_total += ClosedList[i].size();
        nodes_reopen_total += nodes_reopen[i];
        nodes_total += total_region;
        nodes_open_rewrite_total += nodes_open_rewrite[i];
    }
    std::cout << "Sum"
          << "\tOpenList: " << open_list_total
          << "\tHoldingList: " << holding_list_total
          << "\tClosedList: " << closed_list_total
          << "\tReopen: " << nodes_reopen_total
          << "\tTotal: " << nodes_total
          << "\t(Open Rewrite: " << nodes_open_rewrite_total << ")\n";
}

template < int N >
void PAStar<N>::print_answer()
{
    backtrace<N>(ClosedList, m_options.total_regions);
    print_nodes_count();

    std::cout << "Max memory occupied: " << Watcher<N>::getInstance()->getMaxMemoryOccupied()  << std::endl;
}

/*!
 * Same a_star() function usage.
 * Starting function to do a pa_star search.
 */
template < int N >
int PAStar<N>::pa_star(const Node<N> &node_zero, const Coord<N> &coord_final, const PAStarOpt &options)
{
    if (options.threads_num <= 0)
        throw std::invalid_argument("Invalid number of threads");
    Coord<N>::configure_hash(options.hash_type, options.hash_shift);

    PAStar<N> pastar_instance(node_zero, options);
    std::vector<std::thread> threads;
    TimeCounter *t = new TimeCounter("Phase 2: PA-Star running time: ");

    // Create threads
    for (int i = 0; i < options.threads_num; ++i)
        threads.push_back(std::thread(&PAStar::worker, &pastar_instance, i, coord_final));

    // Wait for the end of all threads
    for (auto& th : threads)
        th.join();

    delete t;
    pastar_instance.print_answer();

    if (options.common_options.force_quit)
        exit(0);
    return 0;
}

template<int N>
bool PAStar<N>::updateHighestPriorityRegionOnThread(int tid){


    int active_region = activeRegions[tid];

    int highest_priority_region = activeRegions[tid];
    int highest_priority = getTopPriorityOnRegion(activeRegions[tid]);

    int lowest_priority = -1;
    int lowest_priority_region = -1;


    int other_region_priority;
    for(int rid = tid; rid < m_options.total_regions; rid += m_options.threads_num){
        other_region_priority = getTopPriorityOnRegion(rid);
        if(hasHigherPriority(other_region_priority,highest_priority)){
            highest_priority = other_region_priority;
            highest_priority_region = rid;
        }
        else if(hasHigherPriority(lowest_priority,other_region_priority) && locations[rid] == Location::MEMORY){
            lowest_priority = other_region_priority;
            lowest_priority_region = rid;
        }
    }

    if(lowest_priority_region != highest_priority_region && lowest_priority_region != active_region){
        least_prioritary_regions[tid] = lowest_priority_region;
    }
    else{
        least_prioritary_regions[tid] = -1;
    }

    if(active_region != highest_priority_region){
        activeRegions[tid] = highest_priority_region;
        return true;
    }
    else{
        return false;
    }
}

template<int N>
bool PAStar<N>::hasHigherPriority(int p1, int p2){
    return p1 < p2;
}

template<int N>
int PAStar<N>::getTopPriorityOnRegion(int rid) {
    return std::min(OpenList[rid].get_highest_priority(),holdingList[rid].get_highest_priority());
}

template <int N>
void PAStar<N>::enqueue_holding_list(int rid, std::vector< Node<N> > &nodes){

    for (typename std::vector< Node<N> >::iterator it = nodes.begin() ; it != nodes.end(); ++it){
        holdingList[rid].conditional_insert(*it);
    }
    return;
}

template <int N>
void PAStar<N>::consume_queue_to_holding_list(int rid){
    std::unique_lock<std::mutex> queue_lock(queue_mutex[rid]);
    std::vector< Node<N> > nodes_to_expand(queue_nodes[rid]);
    queue_nodes[rid].clear();
    queue_lock.unlock();

    enqueue_holding_list(rid, nodes_to_expand);
    return;
}

template <int N>
void PAStar<N>::consume_holding_list(int rid){

    std::vector< Node<N> > nodes_to_expand;
    holdingList[rid].consume(nodes_to_expand);
    enqueue(rid, nodes_to_expand);
    return;
}

template< int N>
int PAStar<N>::getLeastPrioritaryRegion(int tid) {
    return least_prioritary_regions[tid];
}



#define PASTAR_DECLARE_TEMPLATE( X ) \
template class PAStar< X >; \

MAX_NUM_SEQ_HELPER(PASTAR_DECLARE_TEMPLATE);
