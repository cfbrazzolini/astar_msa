#ifndef _PSTAR_H
#define _PSTAR_H
/*!
 * \class PAStar
 * \author Daniel Sundfeld
 * \copyright MIT License
 *
 * \brief Do a multiple sequence alignment reducing the search space
 * with parallel a-star algorithm
 */
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <iostream>
#include <map>
#include <string>
#include <thread>
#include <vector>
#include <fstream>
#include <sstream>


#include "AStar.h"
#include "Coord.h"
#include "CoordHash.h"
#include "HoldingList.h"
#include "Node.h"
#include "PriorityList.h"
#include "Watcher.h"
#include "WatcherBase.h"


#ifndef THREADS_NUM
        #define THREADS_NUM std::thread::hardware_concurrency()
#endif

#ifndef REGIONS_PER_THREAD
        #define REGIONS_PER_THREAD 1
#endif

    /*!
     * \brief Arguments for PAStar class
     */
    struct PAStarOpt {
        AStarOpt common_options;
        hashType hash_type;
        int hash_shift;
        int threads_num;
        int regions_per_thread;
        int total_regions;
        unsigned long long memory_limit;
        double memory_threshold;

        PAStarOpt()
        {
            hash_type = HashFZorder;
            hash_shift = HASH_SHIFT;
            threads_num = THREADS_NUM;
            regions_per_thread = REGIONS_PER_THREAD;
            total_regions = threads_num * regions_per_thread;
            memory_limit = 0;
            memory_threshold = 0.9;
        }
        PAStarOpt(AStarOpt &common, hashType type, int shift, int th,int rPT = REGIONS_PER_THREAD, unsigned long long limit = 0, double
        _threshold = 0.8)
        {
            common_options = common;
            hash_type = type;
            hash_shift = shift;
            threads_num = th;
            regions_per_thread = rPT;
            total_regions = th * rPT;
            memory_limit = limit;
            memory_threshold = _threshold;
        }
    };

    enum Location{MEMORY,DISK,MEM_2_DISK,DISK_2_MEM};

    template < int N >
    class PAStar {
    public:
        static int pa_star(const Node<N> &node_zero, const Coord<N> &coord_final, const PAStarOpt &options);

    private:
        // Members
        const PAStarOpt m_options;
        PriorityList<N> *OpenList;
        std::map< Coord<N>, Node<N> > *ClosedList;
        HoldingList<N> *holdingList;
        long long int *nodes_reopen;
        long long int *nodes_open_rewrite;
        std::chrono::duration<int,std::milli> *write_timer;

        std::mutex *queue_mutex;
        std::condition_variable *queue_condition;
        std::vector< Node<N> > *queue_nodes;
        std::vector<int> activeRegions;

        std::atomic<bool> end_cond;


        std::mutex final_node_mutex;
        Node<N> final_node;
        std::atomic<int> final_node_count;

        std::mutex sync_mutex;
        std::atomic<int> sync_count;
        std::condition_variable sync_condition;

        // Watcher variables
        std::mutex *closed_list_mutex;
        std::vector<Location> locations;
        std::vector<int> least_prioritary_regions;
        std::mutex print_mutex;


    // Constructor
        PAStar(const Node<N> &node_zero, const PAStarOpt &opt);
        ~PAStar();

        // Misc functions
        int set_affinity(int tid);
        void sync_threads();
        void print_nodes_count();

        // Queue functions
        void enqueue(int rid, std::vector< Node<N> > &nodes);
        void consume_queue(int rid);
        void wait_queue(int tid);
        void wake_all_queue();

        // End functions
        void process_final_node(int tid, const Node<N> &n);
        bool check_stop(int tid);

        // Worker Functions
        void worker_inner(int tid, const Coord<N> &coord_final);
        int worker(int tid, const Coord<N> &coord_final);

        // Backtrack
        void print_answer();

        //Memory strategy functions
        bool updateHighestPriorityRegionOnThread(int tid);
        int getTopPriorityOnRegion(int rid);
        void enqueue_holding_list(int rid, std::vector< Node<N> > &nodes);
        void consume_queue_to_holding_list(int rid);
        void consume_holding_list(int rid);
        bool hasHigherPriority(int p1,int p2);
        int getLeastPrioritaryRegion(int tid);
        void write(int rid);
        void read(int rid);
        bool check_node_barrier(int rid);
};
#endif
