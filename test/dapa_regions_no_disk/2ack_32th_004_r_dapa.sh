#!/bin/bash
#SBATCH -J dapa-32           # job name
#SBATCH -o Pipeline.o%j       # output and error file name (%j expands to jobID)
#SBATCH -n 1              # total number of mpi tasks requested
#SBATCH -N 1             # total number of mpi tasks requested
#SBATCH -p largemem     # queue (partition) -- normal, development, etc.
#SBATCH -t 06:00:00        # run time (hh:mm:ss) - 1.5 hours
#SBATCH --mail-type=begin  # email me when the job starts
#SBATCH --mail-type=end    # email me when the job finishes
#SBATCH -A TG-ASC130023
#module swap intel gcc/4.7.1
#DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
SEQ="../../../../seqs/Balibase/Ref1/7_long_medium_id/2ack.fasta"
#SEQ="../../seqs/Benchmark/2ack_cutted.fasta"
CMD="../../../../bin/dapa"

TH="32"
THREADS="-t $TH"
REGIONS="004"
DAPA_OPTS="-r $REGIONS -l 53687091200 -o 0.85"
OPT="$THREADS $DAPA_OPTS"

#cd "../../seqs/Benchmark"
cd "../../seqs/Balibase/Ref1/7_long_medium_id/"

rm files
mkdir /tmp/george/
rm /tmp/george/*
ln -s /tmp/george/ files

strace -ve wait4 /usr/bin/time -v $CMD $OPT $SEQ >> $SEQ.$TH.$REGIONS.dapa.nodisk.local.output 2>&1
rm files

mkdir /scratch/02542/gteodoro/files/
rm /scratch/02542/gteodoro/files/*
ln -s /scratch/02542/gteodoro/files/ files

strace -ve wait4 /usr/bin/time -v $CMD $OPT $SEQ >> $SEQ.$TH.$REGIONS.dapa.nodisk.fs.output 2>&1
rm files
