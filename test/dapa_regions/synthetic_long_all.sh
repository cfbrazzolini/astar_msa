#!/bin/bash
#SBATCH -J dapa_synthetic_long_32      # job name
#SBATCH -o Pipeline.o%j       # output and error file name (%j expands to jobID)
#SBATCH -n 1              # total number of mpi tasks requested
#SBATCH -N 1             # total number of mpi tasks requested
#SBATCH -p largemem     # queue (partition) -- normal, development, etc.
#SBATCH -t 06:00:00        # run time (hh:mm:ss) - 1.5 hours
#SBATCH --mail-type=begin  # email me when the job starts
#SBATCH --mail-type=end    # email me when the job finishes
#SBATCH -A TG-ASC130023
#module swap intel gcc/4.7.1

SEQ="../../seqs/Benchmark/synthetic_long.fasta"
CMD="../../bin/dapa"

THREADS="-t 4"
HASH="-y FZORDER"
HASH_SHIFT="-s 12"
DAPA_OPTS="-l 1610612736 -o 0.85"
OPT="$THREADS $HASH $HASH_SHIFT $DAPA_OPTS"

for i in 8 16 32 64 128 256 512; do
    REGION="-r $i"
    rm -rf files
    mkdir files
    strace -ve wait4 /usr/bin/time -v $CMD $REGION $OPT $SEQ >> synthetic_long_dapa_4_r$i.output 2>&1
done
