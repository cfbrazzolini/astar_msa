#!/bin/bash
#SBATCH -J dapa-32           # job name
#SBATCH -o Pipeline.o%j       # output and error file name (%j expands to jobID)
#SBATCH -n 1              # total number of mpi tasks requested
#SBATCH -N 1             # total number of mpi tasks requested
#SBATCH -p largemem     # queue (partition) -- normal, development, etc.
#SBATCH -t 06:00:00        # run time (hh:mm:ss) - 1.5 hours
#SBATCH --mail-type=begin  # email me when the job starts
#SBATCH --mail-type=end    # email me when the job finishes
#SBATCH -A TG-ASC130023
SEQ="../../../../seqs/Balibase/Ref1/8_long_high_id/1gpb.fasta"
CMD="../../../../bin/dapa"

TH="32"
THREADS="-t $TH"
REGIONS="064"
DAPA_OPTS="-r $REGIONS -l 53687091200"

THRESHOLD="0.80 0.75 0.70 0.65 0.60 0.55 0.50 0.45 0.40 0.35 0.30 0.25 0.20 0.15 0.10 0.05"
cd "../../seqs/Balibase/Ref1/8_long_high_id/"

for THRS in $THRESHOLD; do
OPT="$THREADS $DAPA_OPTS -o $THRS"
rm files
mkdir /tmp/george/
rm /tmp/george/*
ln -s /tmp/george/ files

strace -ve wait4 /usr/bin/time -v $CMD $OPT $SEQ >> $SEQ.$TH.$REGIONS.$THRS.dapa.nodisk.local.output 2>&1
done
